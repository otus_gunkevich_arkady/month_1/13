﻿using console_Menu.Component;

namespace console_Menu
{
    public class Menu
    {
        public List<MenuButton> Buttons => _buttons;
        private List<MenuButton> _buttons { get; set; }
        public List<string> Text => _text;
        private List<string> _text { get; set; }
        public Menu(List<MenuButton> buttons, List<string> text)
        {
            _buttons = buttons;
            if (!buttons.Any(s => s.IsSelect))
                buttons.First().IsSelect = true;
            _text = text;
        }
        public void ExecuteButton() => _buttons.SingleOrDefault(s => s.IsSelect).Execute();
        public void UpSelect()
        {
            int _changeIndex = _buttons.IndexOf(_buttons.SingleOrDefault(s => s.IsSelect));
            if (_changeIndex > 0)
            {
                _buttons[_changeIndex - 1].IsSelect = true;
                _buttons[_changeIndex].IsSelect = false;
            }
        }
        public void DownSelect()
        {
            int _changeIndex = _buttons.IndexOf(_buttons.SingleOrDefault(s => s.IsSelect));
            if (_changeIndex < _buttons.Count() - 1)
            {
                _buttons[_changeIndex + 1].IsSelect = true;
                _buttons[_changeIndex].IsSelect = false;
            }
        }
    }
}
