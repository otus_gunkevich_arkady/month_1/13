﻿
namespace console_Menu
{
    public class MenuService : IMenuService
    {
        protected Menu _menu;
        protected bool _exit = false;
        public virtual void Enter() => _menu.ExecuteButton();
        public virtual void Up() => _menu.UpSelect();
        public virtual void Down() => _menu.DownSelect();
        public virtual void Left() { }
        public virtual void Right() { }
        public virtual void Write()
        {       
            Console.Clear();
            _menu.Buttons.ForEach(s =>
            {
                if (s.IsSelect) Console.BackgroundColor = s.ColorSelect;
                Console.WriteLine($"{s.Text}");
                Console.BackgroundColor = ConsoleColor.Black;
            });
            Console.WriteLine("====================================");
            _menu.Text.ForEach(s => Console.WriteLine(s));
        }
        public virtual bool Exit() { return _exit; }
    }
}

