﻿namespace console_Menu.Component
{
    public class MenuButton
    {
        public bool IsSelect { get; set; }
        public string Text { get; private set; }
        public Action Execute { get; private set; }
        public ConsoleColor ColorSelect { get; private set; }
        public MenuButton(string text, Action execute, ConsoleColor colorselect = ConsoleColor.DarkGray)
        {
            IsSelect = false;
            Text = text;
            Execute = execute;
            ColorSelect = colorselect;
        }
    }
}
