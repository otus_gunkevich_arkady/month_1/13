﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace console_Menu
{
    public class MenuVisual
    {
        private IMenuService _menuService;
        public MenuVisual(IMenuService menuService) => _menuService = menuService;
        public void Visual()
        {
            while (!_menuService.Exit())
            {
                _menuService.Write();

                var _key = Console.ReadKey();
                switch (_key.Key)
                {
                    case ConsoleKey.UpArrow: { _menuService.Up(); break; }
                    case ConsoleKey.DownArrow: { _menuService.Down(); break; }
                    case ConsoleKey.LeftArrow: { _menuService.Left(); break; }
                    case ConsoleKey.RightArrow: { _menuService.Right(); break; }
                    case ConsoleKey.Enter: { _menuService.Enter(); break; }
                    default: { break; }
                }
            }
        }
    }
}
