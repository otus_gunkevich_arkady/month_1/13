﻿namespace console_Menu { 
    public interface IMenuService
    {
        void Write();
        void Up();
        void Down();
        void Left();
        void Right();
        void Enter();
        bool Exit();
    }
}
