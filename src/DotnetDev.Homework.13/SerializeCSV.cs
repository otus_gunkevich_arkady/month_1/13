﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace DotnetDev.Homework._13
{
    public class SerializeCSV
    {
        #region Deserialize
        private class NodeT
        {
            public string Name { get; set; }
            public string Types { get; set; }
            public string Value { get; set; }
            public List<NodeT> Node { get; set; }
        }
        private string _delimiter = ";";
        private string _key = "|";
        public T Deserialize<T>(string deserialization) where T : new()
        {
            //Получаем сериализованные поля, сколько бы их небыло
            return (T)GetObject(ParserInfo(deserialization), new T().GetType());
        }
        private object GetObject(List<NodeT> node, Type T, int count = 0)
        {
            if (node is null) return null;
            //Вот серъезно, магия, никак иначе назвать не могу
            Type _type = T;
            if (count > 0)
            {
                var _objectArrayElementType = _type.GetElementType();
                var _objectArray = Array.CreateInstance(_objectArrayElementType, count);
                for (int i = 0; i < count; i++)
                {
                    _objectArray.SetValue(GetObject(node[i].Node, _objectArrayElementType), i);
                }
                return _objectArray;
            }
            else
            {
                var _object = Activator.CreateInstance(_type);
                foreach (var _node in node)
                {
                    if (_node.Types == "value")
                    {
                        var _field = _type.GetField(_node.Name);
                        _field?.SetValue(_object, Convert.ChangeType(_node.Value, _field.FieldType));
                    }
                    if (_node.Types == "object" && _node.Node is not null)
                    {
                        var _field = _type.GetField(_node.Name);
                        _field?.SetValue(_object, GetObject(_node.Node, _field.FieldType));
                    }
                    if (_node.Types == "array" && _node.Node is not null)
                    {
                        var _field = _type.GetField(_node.Name);
                        _field?.SetValue(_object, Convert.ChangeType(GetObject(_node.Node, _field.FieldType, _node.Node.Count), _field.FieldType));
                    }
                }
                return _object;
            }
        }
        private void KeyTrim(string element, out string name, out string elementTrim)
        {
            name = element.Split(_key)[0];
            elementTrim = element.TrimStart(name.ToArray()).TrimStart(_key.ToArray());
        }
        //Опять рекурсия, ну люблю я рекурсии
        private List<NodeT> ParserInfo(string line)
        {
            if (line == "") return null;
            List<NodeT> _elements = new List<NodeT>();
            string _elementAdd = "";
            bool _objectOpen = false;
            int _objectBracked = 0;
            bool _arrayOpen = false;
            int _arrayBracked = 0;
            foreach (char _symbol in line)
            {
                if (_objectOpen)
                {
                    if (_symbol == '{') _objectBracked++;
                    if (_symbol == '}') _objectBracked--;
                    if (_objectBracked == 0)
                    {
                        KeyTrim(_elementAdd, out string _name, out string _elemetnTrim);
                        _elements.Add(new NodeT() { Name = _name, Types = "object", Value = _elementAdd, Node = ParserInfo(_elemetnTrim) });
                        _elementAdd = "";
                        _objectOpen = false;
                    }
                    else
                    {
                        _elementAdd += _symbol.ToString();
                    }
                    continue;
                }
                else if (_arrayOpen)
                {
                    if (_symbol == '[') _arrayBracked++;
                    if (_symbol == ']') _arrayBracked--;
                    if (_arrayBracked == 0)
                    {
                        KeyTrim(_elementAdd, out string _name, out string _elemetnTrim);
                        _elements.Add(new NodeT() { Name = _name, Types = "array", Value = _elementAdd, Node = ParserInfo(_elemetnTrim) });
                        _elementAdd = "";
                        _arrayOpen = false;
                    }
                    else
                    {
                        _elementAdd += _symbol.ToString();
                    }
                    continue;
                }
                else if (_symbol == '{')
                {
                    _objectOpen = true;
                    _objectBracked++;
                    continue;
                }
                else if (_symbol == '[')
                {
                    _arrayOpen = true;
                    _arrayBracked++;
                    continue;
                }
                else if (_symbol == ';')
                {
                    if (_elementAdd == "") continue;
                    KeyTrim(_elementAdd, out string _name, out string _elemetnTrim);
                    _elements.Add(new NodeT() { Name = _name, Types = "value", Value = _elemetnTrim });
                    _elementAdd = "";
                    continue;
                }
                else
                {
                    _elementAdd += _symbol.ToString();
                }
            }
            return _elements;
        }
        #endregion
        #region Serialize
        //Поменял немного алгоритм (в коммитах видно)
        //Я всё ещё остался вроде как на стандарте CSV
        //И при этом могу вроводить сериализацию
        public string Serialize(object? serialization)
        {
            if (serialization is null) return _delimiter;
            Type _typeSerializacion = serialization.GetType();

            string _serializationString = string.Empty;
            if (_typeSerializacion.IsPrimitive)
            {
                _serializationString += $"{serialization}{_delimiter}";
            }
            else if (_typeSerializacion.IsArray)
            {
                int _iterator = 0;
                foreach (var item in (Array)serialization)
                {
                    if (item.GetType().IsClass && !item.GetType().IsArray)
                    {
                        _serializationString += _iterator + _key + "{" + Serialize(item) + "}";
                    }
                    else
                    {
                        _serializationString += _iterator + _key + Serialize(item);
                    }
                    _iterator++;
                }
            }
            else if (_typeSerializacion.IsClass && _typeSerializacion.Name != "String")
            {
                //_serializationString += "{";
                _typeSerializacion.GetFields().ToList().ForEach(s =>
                {
                    _serializationString += s.Name + _key;
                    if (s.FieldType.IsArray)
                    {
                        _serializationString += "[";
                        if (s?.GetValue(serialization) is not null)
                            _serializationString += Serialize(s?.GetValue(serialization));
                        _serializationString += "]";
                    }
                    else if (s.FieldType.IsClass && s.FieldType.Name != "String")
                    {
                        _serializationString += "{";
                        if (s?.GetValue(serialization) is not null)
                            _serializationString += Serialize(s?.GetValue(serialization));
                        _serializationString += "}";
                    }
                    else
                    {
                        _serializationString += $"{s?.GetValue(serialization)}";
                    }
                    _serializationString += _delimiter;
                });
                //_serializationString += "}";
            }

            return _serializationString;
        }
        //Я сделал сначала такой сериализатор
        //Потом понял что нам надо CSV - господи, с таким объектом как class
        //делать сериализатор в формате который пердназначем для матриц, весело
        #endregion
    }
}
