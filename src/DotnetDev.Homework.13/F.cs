﻿namespace DotnetDev.Homework._13
{
    [Serializable]
    public class F
    {
        public int I1;
        public double I2;
        public bool I3;
        public char I4;
        public string I5;
        public F Node;
        public F[] ArrayF;
        public int[] ArrayInt;
        public F()
        {

        }
        public F(int i1, double i2, bool i3, char i4, string i5)
        {
            this.I1 = i1;
            this.I2 = i2;
            this.I3 = i3;
            this.I4 = i4;
            this.I5 = i5;
        }
    }
    //public class F
    //{
    //    public int I1;
    //    public F[] ArrayF;
    //    public F Node;
    //    public F()
    //    {
    //        this.I1 = 1;
    //        this.Node = new F(11);
    //        this.ArrayF = new F[2] { new F(211), new F(221) };
    //    }
    //    public F(int i1)
    //    {
    //        this.I1 = i1;
    //    }
    //}
}
