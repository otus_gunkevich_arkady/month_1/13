﻿using Newtonsoft.Json;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace DotnetDev.Homework._13
{
    public class Demonstration
    {
        private F _objectSerializate;
        private Stopwatch _stopwatch;
        private SerializeCSV _serializeCSV;
        private BinaryFormatter _bynaryFormatter;
        //private SoapFormatter _soapFormatter;
        private XmlSerializer _xmlSerializer;
        private int _count;
        public Demonstration(int count)
        {
            _objectSerializate = GetObjectClass();
            _serializeCSV = new SerializeCSV();
            _count = count;
            _bynaryFormatter = new BinaryFormatter();
            //_soapFormatter = new SoapFormatter();
            _xmlSerializer = new XmlSerializer(typeof(F));
        }
        public F GetObjectClass()
        {
            F _objectReturn = new F();
            _objectReturn.I1 = 1;
            _objectReturn.I2 = 10.56;
            _objectReturn.I3 = false;
            _objectReturn.I4 = 'O';
            _objectReturn.I5 = "Раз Два";
            _objectReturn.Node = new F(11, 635.543, true, 'T', "Три Четыре");
            _objectReturn.ArrayF = new F[2] { new F(211, 121.1, true, 'U', "Пять Шесть"), new F(221, 62.12356, false, 'S', "Семь Восемь") };
            _objectReturn.ArrayInt = new int[3] { 411, 412, 413 };
            return _objectReturn;
        }
        public List<string> Info() => new List<string>() { "", $"Итераций произведено {_count}" };
        public List<string> GetAll()
        {
            List<string> _returnList = new List<string>();

            GetSerializeCSV().ForEach(s => _returnList.Add(s));
            GetDeserializeCSV().ForEach(s => _returnList.Add(s));
            _returnList.Add("");
            GetSerializeNewtonsoftJSON().ForEach(s => _returnList.Add(s));
            GetDeserializeNewtonsoftJSON().ForEach(s => _returnList.Add(s));
            _returnList.Add("");
            GetSerializeBinary().ForEach(s => _returnList.Add(s));
            GetDeserializeBinary().ForEach(s => _returnList.Add(s));
            _returnList.Add("");
            //GetSerializeSoap().ForEach(s => _returnList.Add(s));
            //GetDeserializeSoap().ForEach(s => _returnList.Add(s));
            //_returnList.Add("");
            GetSerializeXML().ForEach(s => _returnList.Add(s));
            GetDeserializeXML().ForEach(s => _returnList.Add(s));

            return _returnList;
        }
        public List<string> GetSerializeAll()
        {
            List<string> _returnList = new List<string>();

            GetSerializeCSV().ForEach(s => _returnList.Add(s));
            _returnList.Add("");
            GetSerializeNewtonsoftJSON().ForEach(s => _returnList.Add(s));
            _returnList.Add("");
            GetSerializeBinary().ForEach(s => _returnList.Add(s));
            _returnList.Add("");
            //GetSerializeSoap().ForEach(s => _returnList.Add(s));
            //_returnList.Add("");
            GetSerializeXML().ForEach(s => _returnList.Add(s));

            return _returnList;
        }
        public List<string> GetDeserializeAll()
        {
            List<string> _returnList = new List<string>();

            GetDeserializeCSV().ForEach(s => _returnList.Add(s));
            _returnList.Add("");
            GetDeserializeNewtonsoftJSON().ForEach(s => _returnList.Add(s));
            _returnList.Add("");
            GetDeserializeBinary().ForEach(s => _returnList.Add(s));
            _returnList.Add("");
            //GetDeserializeSoap().ForEach(s => _returnList.Add(s));
            //_returnList.Add("");
            GetDeserializeXML().ForEach(s => _returnList.Add(s));

            return _returnList;
        }
        public List<string> GetSerializeCSV()
        {
            _stopwatch = Stopwatch.StartNew();
            for (int i = 0; i < _count; i++)
            {
                _serializeCSV.Serialize(_objectSerializate);
            }
            _stopwatch.Stop();
            return GetInfoJob("Сериализация в CSV (рекурсивный метод)", new TimeSpan(_stopwatch.ElapsedTicks / _count));
        }
        public List<string> GetDeserializeCSV()
        {
            string csv = _serializeCSV.Serialize(_objectSerializate);
            _stopwatch = Stopwatch.StartNew();
            for (int i = 0; i < _count; i++)
            {
                _serializeCSV.Deserialize<F>(csv);
            }
            _stopwatch.Stop();
            return GetInfoJob("Десериализация в CSV (рекурсивный метод)", new TimeSpan(_stopwatch.ElapsedTicks / _count));
        }
        public List<string> GetSerializeNewtonsoftJSON()
        {
            _stopwatch = Stopwatch.StartNew();
            for (int i = 0; i < _count; i++)
            {
                JsonConvert.SerializeObject(_objectSerializate);
            }
            _stopwatch.Stop();
            return GetInfoJob("Сериализация в Newtonsoft.Json", new TimeSpan(_stopwatch.ElapsedTicks / _count));
        }
        public List<string> GetDeserializeNewtonsoftJSON()
        {
            string json = JsonConvert.SerializeObject(_objectSerializate); 
            _stopwatch = Stopwatch.StartNew();
            for (int i = 0; i < _count; i++)
            {
                JsonConvert.DeserializeObject<F>(json);
            }
            _stopwatch.Stop();
            return GetInfoJob("Десериализация в Newtonsoft.Json", new TimeSpan(_stopwatch.ElapsedTicks / _count));
        }
        //Прочитал что такой метод небезопасен якобы
        public List<string> GetSerializeBinary()
        {
            _stopwatch = Stopwatch.StartNew();
            using (var _stream = new MemoryStream())
            {
                for (int i = 0; i < _count; i++)
                {
                    _bynaryFormatter.Serialize(_stream, _objectSerializate);
                }
            }
            _stopwatch.Stop();
            return GetInfoJob("Сериализация в BinaryFormatter", new TimeSpan(_stopwatch.ElapsedTicks / _count));
        }
        public List<string> GetDeserializeBinary()
        {
            _stopwatch = Stopwatch.StartNew();
            
            using (var _stream = new MemoryStream())
            {
                _bynaryFormatter.Serialize(_stream, _objectSerializate);
                _stream.Seek(0, SeekOrigin.Begin);
                for (int i = 0; i < _count; i++)
                {
                    _stream.Position = 0;
                    var _buff = _bynaryFormatter.Deserialize(_stream);
                }
            }
            _stopwatch.Stop();
            return GetInfoJob("Десериализация в BinaryFormatter", new TimeSpan(_stopwatch.ElapsedTicks / _count));
        }
        //Он как то долго работает
        //Делаю первый раз, может криво написал реализацию
        //Но этим я надеюсь никогда не буду пользоваться, и поэтому не хочу разбираться
        //Помоему есть реализация XML которая покрывает вобще всё
        //SOAP идёт нахер, не хочу с ним разбираться
        //public List<string> GetSerializeSoap()
        //{
        //    _stopwatch = Stopwatch.StartNew();
        //    using (var _stream = new MemoryStream())
        //    {
        //        for (int i = 0; i < _count; i++)
        //        {
        //            _soapFormatter.Serialize(_stream, _objectSerializate);
        //        }
        //    }
        //    _stopwatch.Stop();
        //    return GetInfoJob("Сериализация в Soap (Работает долго)", new TimeSpan(_stopwatch.ElapsedTicks / _count));
        //}
        //public List<string> GetDeserializeSoap()
        //{
        //    _stopwatch = Stopwatch.StartNew();
        //    using (var _stream = new MemoryStream())
        //    {
        //        _soapFormatter.Serialize(_stream, _objectSerializate);
        //        _stream.Seek(0, SeekOrigin.Begin);
        //        for (int i = 0; i < _count; i++)
        //        {
        //            _stream.Position = 0;
        //            var _buff = (F)_soapFormatter.Deserialize(_stream);
        //        }
        //    }
        //    _stopwatch.Stop();
        //    return GetInfoJob("Десериализация в Soap (Работает долго)", new TimeSpan(_stopwatch.ElapsedTicks / _count));
        //}
        public List<string> GetSerializeXML()
        {
            _stopwatch = Stopwatch.StartNew();
            using (var _stream = new MemoryStream())
            {
                for (int i = 0; i < _count; i++)
                {
                    _xmlSerializer.Serialize(_stream, _objectSerializate);
                }
            }
            _stopwatch.Stop();
            return GetInfoJob("Сериализация в XML", new TimeSpan(_stopwatch.ElapsedTicks / _count));
        }
        public List<string> GetDeserializeXML()
        {
            _stopwatch = Stopwatch.StartNew();
            using (var _stream = new MemoryStream())
            {
                _xmlSerializer.Serialize(_stream, _objectSerializate);
                _stream.Seek(0, SeekOrigin.Begin);
                for (int i = 0; i < _count; i++)
                {
                    _stream.Position = 0;
                    var _buff = (F)_xmlSerializer.Deserialize(_stream);
                }
            }
            _stopwatch.Stop();
            return GetInfoJob("Десериализация в XML", new TimeSpan(_stopwatch.ElapsedTicks / _count));
        }
        private List<string> GetInfoJob(string text, TimeSpan time) =>
            new List<string>() {
                text,
                $"Затраченое время в милисекундах на одну итерацию {time.TotalMilliseconds}"};
    }
}
