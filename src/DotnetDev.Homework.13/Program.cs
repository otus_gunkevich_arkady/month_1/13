﻿using console_Menu;

namespace DotnetDev.Homework._13
{
    public class Program
    {
        private static void Main(string[] args) => new MenuVisual(new Start()).Visual();
    }
}