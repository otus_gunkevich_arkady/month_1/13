﻿using console_Menu;
using console_Menu.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotnetDev.Homework._13
{
    public class Deserialization : MenuService
    {
        private List<MenuButton> _buttons = new List<MenuButton>();
        private List<string> _text = new List<string>();
        private Demonstration _demonstration;
        public Deserialization()
        {
            _demonstration = new Demonstration(100000);
            _buttons.Add(new MenuButton("Свой (CSV)", () => {
                _demonstration.Info().ForEach(s => _text.Add(s));
                _demonstration.GetDeserializeCSV().ForEach(s => _text.Add(s));
                UpdateMenuInfo();
            }));
            _buttons.Add(new MenuButton("Newtonsoft Json", () => {
                _demonstration.Info().ForEach(s => _text.Add(s));
                _demonstration.GetDeserializeNewtonsoftJSON().ForEach(s => _text.Add(s));
                UpdateMenuInfo();
            }));
            _buttons.Add(new MenuButton("Binary", () => {
                _demonstration.Info().ForEach(s => _text.Add(s));
                _demonstration.GetDeserializeBinary().ForEach(s => _text.Add(s));
                UpdateMenuInfo();
            }));
            //_buttons.Add(new MenuButton("SOAP", () => {
            //    _demonstration.Info().ForEach(s => _text.Add(s));
            //    _demonstration.GetDeserializeSoap().ForEach(s => _text.Add(s));
            //    UpdateMenuInfo();
            //}));
            _buttons.Add(new MenuButton("XML", () => {
                _demonstration.Info().ForEach(s => _text.Add(s));
                _demonstration.GetDeserializeXML().ForEach(s => _text.Add(s));
                UpdateMenuInfo();
            }));
            _buttons.Add(new MenuButton("Все", () => {
                _text = new List<string>();
                _demonstration.Info().ForEach(s => _text.Add(s));
                _demonstration.GetDeserializeAll().ForEach(s => _text.Add(s));
                UpdateMenuInfo();
            }));
            _buttons.Add(new MenuButton("Очистить", () => {
                _text = new List<string>();
                UpdateMenuInfo();
            }));

            _buttons.Add(new MenuButton("[*] Назад", () => _exit = true));
            UpdateMenuInfo();
        }
        private void UpdateMenuInfo()
        {
            _menu = new Menu(_buttons, _text);
        }
    }
}
