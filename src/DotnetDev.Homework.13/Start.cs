﻿using console_Menu;
using console_Menu.Component;

namespace DotnetDev.Homework._13
{
    public class Start : MenuService
    {
        private List<MenuButton> _buttons;
        private List<string> _text;
        public Start()
        {
            _buttons = new List<MenuButton>(); 
            _text = new List<string>();
            _buttons.Add(new MenuButton("Вывести задание целиком", () => WriteAll()));
            _buttons.Add(new MenuButton("(Часть 1) Сериализация класса", () => new MenuVisual(new Serialization()).Visual()));
            _buttons.Add(new MenuButton("(Часть 2) Десериализация класса", () => new MenuVisual(new Deserialization()).Visual()));

            _text.Add("");
            _text.Add("");
            _text.Add("Гункевич Аркадий Игоревич | OTUS");
            _text.Add("ДЗ: Демонстрация SOLID принципов");
            _buttons.Add(new MenuButton("[*] Выйти", () => _exit = true));
            UpdateMenu();
        }
        private void UpdateMenu()
        {
            _menu = new Menu(_buttons, _text);
        }
        private void WriteAll()
        {
            Console.WriteLine("Идёт загрузка");
            _text = new List<string>();
            Demonstration _demonstration = new Demonstration(100000);
            _demonstration.Info().ForEach(s => _text.Add(s));
            _text.Add("");
            _demonstration.GetAll().ForEach(s =>  _text.Add(s));
            UpdateMenu();
        }
    }
}
